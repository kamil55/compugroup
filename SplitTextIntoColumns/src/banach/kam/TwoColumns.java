package banach.kam;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class TwoColumns implements SplitText {

	private static List<String> stringList;

	public static int getHalfListSize() {
		return (stringList.size() % 2 != 0) ? stringList.size() / 2 + 1 : stringList.size() / 2;
	}

	public static int getLongestWordValueFromFirstColumn() {
		int maxTextLength = 0;
		for (int i = 0; i < getHalfListSize(); i++) {
			if (maxTextLength < stringList.get(i).length())
				maxTextLength = stringList.get(i).length();
		}
		return maxTextLength + 5;
	}

	@Override
	public boolean isStringValid(String s) {
		if (s.isEmpty() || s == null)
			return false;
		return true;
	}

	@Override
	public void getStringList(String text) {
		int counter = 0;
		String[] tempArray = text.split("\\+");

		for (String s : tempArray) {
			tempArray[counter] = s.trim();
			counter++;
		}
		stringList = new LinkedList<String>(Arrays.asList(tempArray));
	}

	@Override
	public void splitToColumns() {
		int longestWordLength = getLongestWordValueFromFirstColumn();

		if (stringList.size() % 2 != 0) {
			stringList.add("");
		}

		while (!stringList.isEmpty()) {
			String requiredSpaceString = "";
			int requiredSpace = longestWordLength - stringList.get(0).length();

			while (requiredSpace > 0) {
				requiredSpaceString += " ";
				requiredSpace--;
			}
			System.out.print(
					stringList.remove(0) + requiredSpaceString + stringList.remove(stringList.size() / 2) + "\n");
		}
	}

}
