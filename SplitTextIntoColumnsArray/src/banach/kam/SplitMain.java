package banach.kam;

public class SplitMain {
	
	public static void splitText(SplitTextFactory stf, String text) {
		SplitText st = stf.getNumberOfColumns();
		st.getStringArray(text);
		st.splitToColumns();
	}
	
	public static void main(String[] args){
		String text = "I know but what I'm+ trying to achieve + was to not come up an +error message"
				+ " when+ an empty input+ was used, instead just print+ a statement and carry on with+ the iteration."
				+ " I have been answered +now though so thank you! ";
		
		try{
		splitText(new TwoColumnsFactory(), text);
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}

}
