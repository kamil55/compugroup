package banach.kam;

public class TwoColumns implements SplitText {

	private static String[] array;

	public static int getHalfArrayLength() {
		return (array.length % 2 != 0) ? array.length / 2 + 1 : array.length / 2;
	}
	
	public int getLongestWordValueFromFirstColumn() {
		int maxWordLength = 0;
		for (int i = 0; i < getHalfArrayLength(); i++) {
			if (maxWordLength < array[i].length())
				maxWordLength = array[i].length();
		}
		return maxWordLength + 3;
	}

	@Override
	public void getStringArray(String text) {
		array = text.split("\\+");
		for (int i = 0; i < array.length; i++){
			array[i] = array[i].trim();
		}
	}

	@Override
	public void splitToColumns() {
		String requiredSpaceString = "";
		for (int i = 0; i < array.length / 2; i++) {
			int requiredSpace = getLongestWordValueFromFirstColumn() - array[i].length();
			while (requiredSpace > 0) {
				requiredSpaceString += " ";
				requiredSpace--;
			}
			System.out.print(array[i] + requiredSpaceString + array[getHalfArrayLength() + i] + "\n");
			requiredSpaceString = "";
		}

		if (array.length % 2 != 0) {
			System.out.print(array[array.length / 2]);
		}
	}

}
