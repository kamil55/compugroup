package banach.kam;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class TwoColumns2 implements SplitText {

	private static List<String> list = new LinkedList<String>();

	public static int getHalfArrayLength() {
		return (list.size() % 2 != 0) ? list.size() / 2 + 1 : list.size() / 2;
	}
	
	public int getLongestWordValueFromFirstColumn() {
		int maxWordLength = 0;
		for (int i = 0; i < getHalfArrayLength(); i++) {
			if (maxWordLength < list.get(i).length())
				maxWordLength = list.get(i).length();
		}
		return maxWordLength + 3;
	}

	@Override
	public void getStringArray(String text) {
		list = Arrays.asList(text.split("\\+"));
		for (int i = 0; i < list.size(); i++){
			list.add(i, list.get(i).trim());
		}
	}

	@Override
	public void splitToColumns() {
		String requiredSpaceString = "";
		for (int i = 0; i < array.length / 2; i++) {
			int requiredSpace = getLongestWordValueFromFirstColumn() - array[i].length();
			while (requiredSpace > 0) {
				requiredSpaceString += " ";
				requiredSpace--;
			}
			System.out.print(array[i] + requiredSpaceString + array[getHalfArrayLength() + i] + "\n");
			requiredSpaceString = "";
		}

		if (array.length % 2 != 0) {
			System.out.print(array[array.length / 2]);
		}
	}

}
