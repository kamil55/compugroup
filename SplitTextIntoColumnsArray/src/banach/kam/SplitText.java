package banach.kam;

public interface SplitText {
	
	void splitToColumns();
	void getStringArray(String text);

}
