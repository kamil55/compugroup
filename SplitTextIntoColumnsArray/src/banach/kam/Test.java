package banach.kam;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Test {
	private static List<String> stringList;

	public static boolean isStringValid(String s) {
		if (s.isEmpty() || s == null)
			return false;

		return true;
	}

	public static int getHalfListSize() {
		return (stringList.size() % 2 != 0) ? stringList.size() / 2 + 1 : stringList.size() / 2;
	}

	public static void getStringList(String text) {
		int counter = 0;
		String[] tempArray = text.split("\\+");
		
		for(String s : tempArray) {
			tempArray[counter] = s.trim();
			counter++;
		}
		
		stringList = new LinkedList<String>(Arrays.asList(tempArray));
	}

	public static int getLongestWordValueFromFirstColumn() {
		int maxTextLength = 0;
		for (int i = 0; i < getHalfListSize(); i++) {
			if (maxTextLength < stringList.get(i).length())
				maxTextLength = stringList.get(i).length();
		}
		return maxTextLength + 5;
	}

	public static void splitToColumns() {
		int longestWordLength = getLongestWordValueFromFirstColumn();
		
		if (stringList.size() % 2 != 0) {
			stringList.add("");
		}

		while (!stringList.isEmpty()) {
			String requiredSpaceString = "";
			int requiredSpace = longestWordLength - stringList.get(0).length();
	
			while (requiredSpace > 0) {
				requiredSpaceString += " ";
				requiredSpace--;
			}
			System.out.print(
					stringList.remove(0) + requiredSpaceString + stringList.remove(stringList.size() / 2) + "\n");
		}
	}

	public static void splitText(String text) {
		// SplitText st = stf.getNumberOfColumns();
		if (isStringValid(text) == false) {
			System.out.println("Z�y format");
		} else {
			System.out.println("Dobry format");
		}

	}

	public static void main(String[] args) {

		String text = "Blog od wielu innych stron internetowych r�ni si�+ zawarto�ci�. Niegdy� weblogi uto�samiano "
				+ "ze stronami osobistymi +(czy domowymi)[1]. Dzi� ten pogl�d+ jest nieaktualny, wci�� jednak od innych "
				+ "stron internetowych blogi odr�nia bardziej +personalny charakter tre�ci: cz�ciej +stosowana jest narracja "
				+ "pierwszoosobowa, a fakty nierzadko +przeplataj� si� z opiniami autora[2]. Ponadto +mo�na spotka� si� z definicj� "
				+ "bloga jako sposobu +komunikacj";
		getStringList(text);
		splitToColumns();

	}
}
