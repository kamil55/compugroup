package banach.kam;

public interface SplitTextFactory {
	
	SplitText getNumberOfColumns();

}
