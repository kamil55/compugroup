package banach.kam;

public interface SplitText {
	
	void splitToColumns();
	void getStringList(String text);
	boolean isStringValid(String s);
	boolean isColumnWidthValid(int columnWidth);
	void setColumnWidth(int columnWidth);

}
