package banach.kam;

import java.util.LinkedList;
import java.util.List;

public class TwoColumns implements SplitText {

	private static String[] tempArray;
	private static List<String> stringList = new LinkedList<String>();
	private static int columnWidth;

	public int getHalfArrayLength() {
		return (tempArray.length % 2 != 0) ? tempArray.length / 2 + 1 : tempArray.length / 2;
	}

	public static int getLongestTextValueFromFirstColumn() {
		int maxTextLength = 0;
		for (int i = 0; i < stringList.size() / 2; i++) {
			if (maxTextLength < stringList.get(i).length())
				maxTextLength = stringList.get(i).length();
		}
		return maxTextLength + 5;
	}

	@Override
	public void setColumnWidth(int columnWidth) {
		TwoColumns.columnWidth = columnWidth;
	}

	@Override
	public boolean isStringValid(String s) {
		if (s.isEmpty() || s == null)
			return false;
		return true;
	}

	@Override
	public void getStringList(String text) {
		int counter = 0;
		int spaceLeft = columnWidth + 1;
		String tempString = "";
		tempArray = text.split(" ");

		while (counter < tempArray.length) {
			for (int i = counter; i < tempArray.length; i++) {
				if (tempArray[i].length() < spaceLeft) {
					tempString += tempArray[i] + " ";
					spaceLeft -= tempArray[i].length() + 1;
					counter++;
				} else
					break;
			}
			stringList.add(tempString);
			spaceLeft = columnWidth + 1;
			tempString = "";
		}
	}

	@Override
	public void splitToColumns() {
		int longestWordLength = getLongestTextValueFromFirstColumn();

		while (!stringList.isEmpty()) {
			String requiredSpaceString = "";
			int requiredSpace = longestWordLength - stringList.get(0).length();

			while (requiredSpace > 0) {
				requiredSpaceString += " ";
				requiredSpace--;
			}
			System.out.print(
					stringList.remove(0) + requiredSpaceString + stringList.remove(stringList.size() / 2) + "\n");
		}
	}

	@Override
	public boolean isColumnWidthValid(int columnWidth) {
		if(columnWidth < 10)
			return false;
		return true;
	}

}
